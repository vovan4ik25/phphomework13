<?php
	class Control{
        protected $id;
        protected $role;
		protected $firstName;
		protected $lastName;
		protected $phone;
		protected $email;
		
		public function __construct($role, $firstName, $lastName, $phone, $email){
			$this->role = $role;
			$this->firstName = $firstName;
			$this->lastName = $lastName;
			$this->phone = $phone;
			$this->email = $email;
		}

		public static function getInstance($id, PDO $pdo){//метод фабрика
            try{
			$sql1 = 'SELECT * FROM peoples WHERE id=:id';
			$stmt = $pdo->prepare($sql1);
			$stmt->bindValue(':id', $id);
			$stmt->execute();
			$result = $stmt->fetchObject();
            }catch(PDOException $e){
                 	echo "Ошибка получения данных: ".$e->getMessage();
                 	exit();
            }
			if(empty($result)) {
				return null;
			}
			if($result->role == 'administrator'){
				$people = new Administrator(
					$result->role,
					$result->first_name,
					$result->last_name,
					$result->phone,
					$result->email,
					$result->schedule
				);
			}else if($result->role == 'teacher'){
				$people = new Teacher(
					$result->role,
					$result->first_name,
					$result->last_name,
					$result->phone,
					$result->email,
					$result->subject
				);
			}else if($result->role == 'student'){
				$people = new Student(
					$result->role,
					$result->first_name,
					$result->last_name,
					$result->phone,
					$result->email,
					$result->rating,
					$result->visit
				);
			}
			$people->setID($result->id);
			return $people;
		}

		public function setID($id){//метод установки id
			$this->id = $id;
		}

		public function getFullName(){//метод возврата имени
			return $this->lastName." ".$this->firstName;
		}

		public function getVisitCard(){//метод возврата визитки
            $card = "<tr>";
			$card .= "<td>".$this->role."</td>";
			$card .= "<td>".$this->getFullName()."</td>";
			$card .= "<td>".$this->phone."</td>";
			$card .= "<td>".$this->email."</td>";
           	return $card;
		}

        public static function displayPeoples($peoples, $role){//метод обработки выбора категории и вывода на экран
            $div = "<div>";
            if($role=="all"){
                $div .= "<h3>Полный список</h3>";
                foreach ($peoples as $people){
                    $div .= $people->getVisitCard();
                }
            }elseif($role=="student"){
                $div .= "<h3>Студенты</h3>";
                foreach ($peoples as $people){
                    if ($people->role == 'student'){
                            $div .= $people->getVisitCard();
                    }
                }
            }elseif($role=="teacher"){
                $div .= "<h3>Преподаватели</h3>";
                foreach ($peoples as $people){
                    if($people->role == 'teacher'){
                        $div .= $people->getVisitCard();
                    }
                }
            }elseif($role=="administrator"){
                $div .= "<h3>Администраторы</h3>";
                foreach ($peoples as $people){
                    if($people->role == 'administrator'){
                        $div .= $people->getVisitCard();
                    }
                }
            }
            $div .= "</div>";
            return $div;
        }

	}
?>	