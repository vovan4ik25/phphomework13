<?php
	class Teacher extends Control{
		protected $subject;
		
		public function __construct($role, $firstName, $lastName, $phone, $email, $subject){
			parent::__construct($role, $firstName, $lastName, $phone, $email);
			$this->subject = $subject;
			
		}
		
		public function getVisitCard(){//метод возврата визитки
			$card = parent::getVisitCard();
            $card .= "<td>".' - '."</td>";
            $card .= "<td>".' - '."</td>";
			$card .= "<td>".$this->subject."</td>";
            $card .= "<td>".' - '."</td>";
            $card .= "<tr>";
			return $card;
		}

	}
?>	