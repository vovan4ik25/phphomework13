<?php
	class Administrator extends Control{
		protected $schedule;
		
		public function __construct($role, $firstName, $lastName, $phone, $email, $schedule){
			parent::__construct($role, $firstName, $lastName, $phone, $email);
			$this->schedule = $schedule;
			
		}
		
		public function getVisitCard(){//метод возврата визитки
			$card = parent::getVisitCard();
            $card .= "<td>".' - '."</td>";
            $card .= "<td>".' - '."</td>";
            $card .= "<td>".' - '."</td>";
			$card .= "<td>".$this->schedule."</td>";
            $card .= "<tr>";
			return $card;
		}

	}
?>	