<?php 
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(E_ALL);
	
	require_once "db_connect.php";//соединение с БД
	require_once "Control.class.php";//родительский класс
	require_once "Student.class.php";//класс для Student
	require_once "Teacher.class.php";//класс для Teacher
	require_once "Administrator.class.php";//класс для Administrator

    try{
        $sql = 'SELECT id FROM peoples';//вызов id из таблици peoples
        $results = $pdo->query($sql);
    }catch(PDOException $e){
        echo "Ошибка получения данных: ".$e->getMessage();
        exit();
    }

    $peoples = array();
	foreach ($results as $result){//создание объектов класса методом фабрика
	    $peoples[] = Control::getInstance($result['id'], $pdo);
	}

    $role = $_POST['role'];//получение выбора роли людей
	 ?>

<!DOCTYPE html>
<html>
<!-- HEADER START -->
<head>
	<title>Homework #13</title>

		<meta charset="utf-8">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<!-- HEADER END -->
<body>
<!-- CONTENT START -->	
	<div style="height: 900px; margin: 20px">
		<h1>Homework #13</h1>
	<div>
		<h4>Выбрать категорию людей</h4>
		<form action="index.php" method="post"><!-- выбор роли людей -->
    		<select name="role">
       			<option value="all">Все</option>
        		<option value="student">Студенты</option>
        		<option value="teacher">Преподаватели</option>
                <option value="administrator">Администраторы</option>
    		</select>
    		<input type="submit" name="send" value="Выбрать"/>
		</form>
	</div>
	<!-- вывод людей в таблице -->
        <table class="table table-bordered" style="margin: 20px; width: 50%">
            <tr style="background-color: palegreen">
                <th>Role</th>
                <th>Full Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>MidRating</th>
                <th>MidVisit, %</th>
                <th>Subject</th>
                <th>Schedule</th>
            </tr>
             <?=Control::displayPeoples($peoples, $role)?><!-- метод обработки выбора роли и показа данных -->
        </table>
	</div>
	<!-- CONTENT END -->
	<div id="footer">
		<div class="panel panel-default" style="background-color: green">
			<div class="panel-body text-center">
				Shapovalov (c) 2017
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>